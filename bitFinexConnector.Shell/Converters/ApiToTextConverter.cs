﻿using System;
using System.Windows.Data;
using bitFinexConnector.Shell.ViewModels;

namespace bitFinexConnector.Shell.Converters
{
    public class ApiToTextConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            //это можно задекорировать в API enum
            if ((API)value == API.REST)
                return "Get via REST";
            if ((API)value == API.WS)
                return "Get via WS";
            return "Error selecting API";
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
