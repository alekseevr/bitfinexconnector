﻿using System;
using System.Collections.Generic;
using bitFinexConnector.Shell.Models;
using Catel.MVVM;
using Catel.Data;
using Catel.Services;
using System.Threading.Tasks;
using BitfinexConnector;
using System.Collections.ObjectModel;

namespace bitFinexConnector.Shell.ViewModels
{
    public class TradeViewModel : ViewModelBase
    {
        public string Pair
        {
            get { return GetValue<string>(PairProperty); }
            set { SetValue(PairProperty, value); }
        }

        public int MaxCount
        {
            get { return GetValue<int>(MaxCountProperty); }
            set { SetValue(MaxCountProperty, value); }
        }

        public static readonly PropertyData MaxCountProperty = RegisterProperty("MaxCount", typeof(int));
        public static readonly PropertyData PairProperty = RegisterProperty("Pair", typeof(string));


        public ObservableCollection<Trade> Trades
        {
            get { return GetValue<ObservableCollection<Trade>>(TradesProperty); }
            set { SetValue(TradesProperty, value); }
        }

        public static readonly PropertyData TradesProperty = RegisterProperty(nameof(Trades), typeof(ObservableCollection<Trade>), null);
        private readonly IMessageService _messageService;

        public TradeViewModel(IMessageService messageService, API currentApi)
        {
            Model.connector.UnsubscribeAll();
            Pair = "tBTCUSD"; //по умолчанию
            MaxCount = 10; //по умолчанию
            CurrentApi = currentApi;
            _messageService = messageService;
            Trades = new ObservableCollection<Trade>();

            //для последующей валидации
            //GetTrades = CommandHelper.CreateCommand(OnGetTradesExecute, () => PersonValidationSummary);
        }

        public API CurrentApi
        {
            get { return GetValue<API>(CurrentApiProperty); }
            set { SetValue(CurrentApiProperty, value); }
        }

        public static readonly PropertyData CurrentApiProperty = RegisterProperty(nameof(CurrentApi), typeof(API), null);
        public override string Title { get { return "View model title"; } }

        // TODO: Register models with the vmpropmodel codesnippet
        // TODO: Register view model properties with the vmprop or vmpropviewmodeltomodel codesnippets
        // TODO: Register commands with the vmcommand or vmcommandwithcanexecute codesnippets
        
        protected override async Task InitializeAsync()
        {
            await base.InitializeAsync();

            // TODO: subscribe to events here
        }

        protected override async Task CloseAsync()
        {
            // TODO: unsubscribe from events here

            await base.CloseAsync();
        }

        private void OnTradeReceived(Trade trade)
        {
            App.Current.Dispatcher.Invoke((Action)delegate
            {
                Trades.Add(trade);
            });
        }

        public Command GetTrades
        {
            get
            {
                //при валидации убирается
                return new Command(async () =>
                { 
                    if (CurrentApi == API.REST)
                    {
                        var tradesGather = Model.connector.GetNewTradesAsync(Pair, MaxCount);
                        IEnumerable<Trade> trades = await tradesGather;
                        foreach (var trade in trades)
                        {
                            App.Current.Dispatcher.Invoke((Action)delegate
                            {
                                Trades.Add(trade);
                            });
                        }
                    }
                    else if (CurrentApi == API.WS)
                    {
                        Model.connector.NewBuyTrade += OnTradeReceived;
                        Model.connector.SubscribeTrades(Pair, MaxCount);
                    }
                    else
                    {
                        throw new Exception("API error");
                    }
                });
            }
            private set { }
        }

        //для валидации
        //private async void OnGetTradesExecute()
        //{
        //    // TODO: Handle command logic here
        //    var tradesGather = Model.connector.GetNewTradesAsync(Pair, MaxCount);
        //    try
        //    {
        //        Trades = await tradesGather;
        //    }
        //    catch (Exception e)
        //    {
        //        await _messageService.ShowWarningAsync(e.StackTrace);
        //    }
        //}

        //[ValidationToViewModel(Tag = "PersonValidation")]
        //public IValidationSummary PersonValidationSummary { get; set; }

        //protected override void ValidateFields(List<IFieldValidationResult> validationResults)
        //{
        //    if (!string.IsNullOrEmpty(FirstName))
        //    {
        //        validationResults.Add(FieldValidationResult.CreateError(FirstNameProperty, "First name cannot be empty"));
        //    }
        //}
    }
}
