﻿using Catel.MVVM;
using System.Threading.Tasks;
using Catel.Data;
using System.Collections.Generic;
using BitfinexConnector;
using bitFinexConnector.Shell.Models;
using System;
using System.Collections.ObjectModel;

namespace bitFinexConnector.Shell.ViewModels
{

    public class CandleViewModel : ViewModelBase
    {
        public CandleViewModel(API currentApi)
        {
            CurrentApi = currentApi;
            Model.connector.UnsubscribeAll();
            //по умолчанию
            Pair = "tBTCUSD";
            PeriodInSec = 60;
            Candles = new ObservableCollection<Candle>();
        }

        public API CurrentApi
        {
            get { return GetValue<API>(CurrentApiProperty); }
            set { SetValue(CurrentApiProperty, value); }
        }

        public static readonly PropertyData CurrentApiProperty = RegisterProperty(nameof(CurrentApi), typeof(API), null);


        public ObservableCollection<Candle> Candles
        {
            get { return GetValue<ObservableCollection<Candle>>(CandlesProperty); }
            set { SetValue(CandlesProperty, value); }
        }

        public static readonly PropertyData CandlesProperty = RegisterProperty(nameof(Candles), typeof(ObservableCollection<Candle>), null);

        public string Pair
        {
            get { return GetValue<string>(PairProperty); }
            set { SetValue(PairProperty, value); }
        }

        public static readonly PropertyData PairProperty = RegisterProperty(nameof(Pair), typeof(string), null);

        public int PeriodInSec
        {
            get { return GetValue<int>(PeriodInSecProperty); }
            set { SetValue(PeriodInSecProperty, value); }
        }

        public static readonly PropertyData PeriodInSecProperty = RegisterProperty(nameof(PeriodInSec), typeof(int), null);
        public override string Title { get { return "View model title"; } }

        // TODO: Register models with the vmpropmodel codesnippet
        // TODO: Register view model properties with the vmprop or vmpropviewmodeltomodel codesnippets
        // TODO: Register commands with the vmcommand or vmcommandwithcanexecute codesnippets
        
        protected override async Task InitializeAsync()
        {
            await base.InitializeAsync();

            // TODO: subscribe to events here
        }

        protected override async Task CloseAsync()
        {
            // TODO: unsubscribe from events here

            await base.CloseAsync();
        }

        private void OnCandleReceived(Candle candle)
        {
            App.Current.Dispatcher.Invoke((Action)delegate
            {
                Candles.Add(candle);
            });
        }
        public Command GetCandles
        {
            get
            {
                return new Command(async () =>
                {
                    if (CurrentApi == API.REST)
                    {
                        var candlesGather = Model.connector.GetCandleSeriesAsync(Pair, PeriodInSec, null, null);
                        IEnumerable<Candle> candles = await candlesGather;
                        foreach (var candle in candles)
                        {
                            App.Current.Dispatcher.Invoke((Action)delegate
                            {
                                Candles.Add(candle);
                            });
                        }
                    }
                    else if (CurrentApi == API.WS)
                    {
                        Model.connector.CandleSeriesProcessing += OnCandleReceived;
                        Model.connector.SubscribeCandles(Pair, PeriodInSec, null, null);
                    }
                    else
                    {
                        throw new Exception("API error");
                    }
                });
            }
            private set { }
        }
    }
}
