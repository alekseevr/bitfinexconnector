﻿using Catel.Data;
using Catel.Services;
using Catel.MVVM;
using System.Threading.Tasks;

namespace bitFinexConnector.Shell.ViewModels
{
    public enum API
    {
        REST,
        WS
    }

    public class MainWindowViewModel : ViewModelBase
    {
        //private Dictionary<string, IViewModel> _slides;
        IMessageService _messageService;
        public MainWindowViewModel(IMessageService messageService)
        {
            //_slides = new Dictionary<string, IViewModel>()
            //{
            //    { "candle", new CandleViewModel()},
            //    { "trade", new TradeViewModel(messageService)}
            //};
            _messageService = messageService;
            CurrentApi = API.REST;
            //CurrentSlide = _slides["candle"];
            CurrentSlide = new TradeViewModel(messageService, CurrentApi);
        }

        public override string Title { get { return "bitFinexConnector.Shell"; } }

        // TODO: Register models with the vmpropmodel codesnippet
        // TODO: Register view model properties with the vmprop or vmpropviewmodeltomodel codesnippets
        // TODO: Register commands with the vmcommand or vmcommandwithcanexecute codesnippets

        protected override async Task InitializeAsync()
        {
            await base.InitializeAsync();

            // TODO: subscribe to events here
        }

        protected override async Task CloseAsync()
        {
            // TODO: unsubscribe from events here

            await base.CloseAsync();
        }

        public API CurrentApi
        {
            get { return GetValue<API>(CurrentApiProperty); }
            set
            {
                SetValue(CurrentApiProperty, value);
            }
        }

        public static readonly PropertyData CurrentApiProperty = RegisterProperty(nameof(CurrentApi), typeof(API), null);
        public IViewModel CurrentSlide
        {
            get { return GetValue<IViewModel>(CurrentSlideProperty); }
            set { SetValue(CurrentSlideProperty, value); }
        }

        public static readonly PropertyData CurrentSlideProperty = RegisterProperty("CurrentSlide", typeof(IViewModel));

        public Command ShowCandlesSlide
        {
            get
            {
                //return new Command(() => { CurrentSlide = _slides["candle"]; });
                return new Command(() => { CurrentSlide = new CandleViewModel(CurrentApi); });
            }
        }
        public Command ShowTradesSlide
        {
            get
            {
                //return new Command(() => { CurrentSlide = _slides["trade"]; });
                return new Command(() => { CurrentSlide = new TradeViewModel(_messageService, CurrentApi); });
            }
        }
    }
}
