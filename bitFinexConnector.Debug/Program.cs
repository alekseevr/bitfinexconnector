﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BitfinexConnector;

namespace BitFinexConnector.Debug
{
    class Program
    {
        private static BitfinexConnector connector = null;
        static Program()
        {
            connector = new BitfinexConnector();
        }

        private static void OnCandleReceived(Candle candle)
        {
            Console.WriteLine($"Candle (Pair: {candle.Pair}) --> [{candle.OpenTime.LocalDateTime}] High : {candle.HighPrice} Low : {candle.LowPrice} Open : {candle.OpenPrice} Close : {candle.ClosePrice}");
        }

        private static void OnTradeReceived(Trade trade)
        {
            Console.WriteLine($"Trade (Pair: {trade.Pair}, Type: {trade.Type}) --> [{trade.Time.LocalDateTime}] Amount: {trade.Amount}, Price: {trade.Price}");
        }

        private static async void RestApiGathering()
        {
            //REST API
            Task<IEnumerable<Trade>> tradesGather = connector.GetNewTradesAsync("tBTCUSD", 0);
            IEnumerable<Trade> trades = await tradesGather;
            foreach (var trade in trades)
            {
                OnTradeReceived(trade);
                //Console.WriteLine($"Trade (Pair: {trade.Pair}, Type: {trade.Type}) --> [{trade.Time.LocalDateTime}] Amount: {trade.Amount}, Price: {trade.Price}");
            }

            Task<IEnumerable<Candle>> candlesGather = connector.GetCandleSeriesAsync("tBTCUSD", 60, null, null, 5);
            IEnumerable<Candle> candles = await candlesGather;
            foreach (var candle in candles)
            {
                OnCandleReceived(candle);
                //Console.WriteLine($"Candle (Pair: {candle.Pair}) --> [{candle.OpenTime.LocalDateTime}] High : {candle.HighPrice} Low : {candle.LowPrice} Open : {candle.OpenPrice} Close : {candle.ClosePrice}");
            }
        }

        private static void Subscribe()
        {
            //WS API
            connector.CandleSeriesProcessing += OnCandleReceived;
            connector.SubscribeCandles("tBTCUSD", 600, null, null, 5);
            //connector.SubscribeCandles("tETHUSD", 600, null, null, 5);
            //connector.SubscribeCandles("tETHUSD", 60, null, null, 5);

            connector.NewBuyTrade += OnTradeReceived;
            connector.SubscribeTrades("tBTCUSD");
            //connector.SubscribeTrades("tETHUSD");
        }

        private static void Main(string[] args)
        {
            Console.WriteLine("main");
            //RestApiGathering();
            Subscribe();
            Console.WriteLine("back in main");
            Task.Delay(7000).Wait();
            connector.UnsubscribeTrades("tBTCUSD");
            Task.Delay(7000).Wait();
            connector.UnsubscribeCandles("tBTCUSD");
            Console.ReadLine();
        }
    }
}
