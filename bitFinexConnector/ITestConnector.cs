﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BitfinexConnector
{
    interface ITestConnector
    {
        Task<IEnumerable<Trade>> GetNewTradesAsync(string pair, int maxCount);
        Task<IEnumerable<Candle>> GetCandleSeriesAsync(string pair, int periodInSec, DateTimeOffset? from, DateTimeOffset? to = null, long? count = 0);

        //я не нашел среди полей класса Trade в bitfinex API каких либо данных,
        //по которым можно отличить sell trade от buy trade. поэтому только один event
        event Action<Trade> NewBuyTrade;
        //event Action<Trade> NewSellTrade;

        void SubscribeTrades(string pair, int maxCount = 100);
        void UnsubscribeTrades(string pair);

        event Action<Candle> CandleSeriesProcessing;

        //если не отписаться от всех каналов перед новым созданием viewmodel,
        //то будут проблемы с получением первого snapshot.
        void UnsubscribeAll();

        //у меня count, from и to дальше этого места не уходят.
        //то же самое с maxcount для Trades
        //см. комментарии Websocket/Responces/Candles/Candles.cs и Trade.cs
        void SubscribeCandles(string pair, int periodInSec, DateTimeOffset? from = null, DateTimeOffset? to = null, long? count = 0);
        void UnsubscribeCandles(string pair);
    }
}
