﻿using System;
using TestHQ;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace bitFinexConnector
{
    class Program
    {
        static BitfinexConnector connector = null;
        static Program()
        {
            connector = new BitfinexConnector();
        }
        public static async void LaunchGathering()
        {
            Task<IEnumerable<Trade>> tradesGather = connector.GetNewTradesAsync("tBTCUSD", 10);
            IEnumerable<Trade> trades = await tradesGather;
            Console.WriteLine("trades (id):");
            foreach (var t in trades)
            {
                Console.WriteLine(t.Id);
            }

            Task<IEnumerable<Candle>> candlesGather = connector.GetCandleSeriesAsync("tBTCUSD", 60, null, null, 5);
            IEnumerable<Candle> candles = await candlesGather;
            Console.WriteLine("candles (volume):");
            foreach (var c in candles)
            {
                Console.WriteLine(c.TotalVolume);
            }
        }
        static void Main(string[] args)
        {
            Console.WriteLine("main");
            LaunchGathering();
            Console.WriteLine("back in main");
            Console.ReadLine();
        }
    }
}
