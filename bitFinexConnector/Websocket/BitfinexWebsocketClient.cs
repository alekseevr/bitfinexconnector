﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using BitfinexConnector.Json;
using Websocket.Client;
using BitfinexConnector.Websocket.Responses.Configurations;
using BitfinexConnector.Websocket.Requests.Configurations;

namespace BitfinexConnector.Websocket
{
    /// <summary>
    /// Bitfinex websocket client, it wraps `IBitfinexCommunicator` and parse raw data into streams.
    /// Send subscription requests (for example: `new TradesSubscribeRequest(pair)`) and subscribe to `Streams`
    /// </summary>
    public class BitfinexWebsocketClient : IDisposable
    {
        private readonly IWebsocketClient _communicator;
        private readonly IDisposable _messageReceivedSubscription;
        //private readonly IDisposable _configurationSubscription;

        private readonly BitfinexChannelList _channelIdToHandler = new BitfinexChannelList();
        private readonly BitfinexPublicHandler _publicHandler;

        /// <inheritdoc />
        public BitfinexWebsocketClient(IWebsocketClient communicator)
        {
            _communicator = communicator;
            _messageReceivedSubscription = _communicator.MessageReceived.Subscribe(HandleMessage);
            //_configurationSubscription = Streams.ConfigurationSubject.Subscribe(HandleConfiguration);

            _publicHandler = new BitfinexPublicHandler(Streams, _channelIdToHandler);
        }


        public BitfinexClientStreams Streams { get; } = new BitfinexClientStreams();

        public ConfigurationState Configuration { get; private set; } = new ConfigurationState();

        public void Dispose()
        {
            _messageReceivedSubscription?.Dispose();
            //_configurationSubscription?.Dispose();
        }

        public async Task Send<T>(T request)
        {
            try
            {
                var serialized = JsonConvert.SerializeObject(request, BitfinexJsonSerializer.Settings);
                await _communicator.Send(serialized).ConfigureAwait(false);
            }
            catch
            {
                throw;
            }
        }

        private void HandleConfiguration(ConfigurationResponse response)
        {
            try
            {
                if (!response.IsConfigured || !response.Flags.HasValue)
                {
                    Configuration = new ConfigurationState();
                    return;
                }

                var flags = response.Flags.Value;
                Configuration = new ConfigurationState(
                    (flags & (int)ConfigurationFlag.DecimalAsString) > 0,
                    (flags & (int)ConfigurationFlag.TimeAsString) > 0,
                    (flags & (int)ConfigurationFlag.Timestamp) > 0,
                    (flags & (int)ConfigurationFlag.Sequencing) > 0,
                    (flags & (int)ConfigurationFlag.Checksum) > 0
                    );
            }
            catch
            {
                throw;
            }
        }

        private void HandleMessage(string message)
        {
            try
            {
                var formatted = (message ?? string.Empty).Trim();

                if (formatted.StartsWith("{"))
                {
                    _publicHandler.OnObjectMessage(formatted);
                    return;
                }

                if (formatted.StartsWith("["))
                {
                    OnArrayMessage(formatted);
                    return;
                }
            }
            catch
            {
                throw;
            }
        }

        private void OnArrayMessage(string msg)
        {
            var parsed = JsonConvert.DeserializeObject<JArray>(msg);
            if (parsed.Count() < 2)
            {
                return;
            }

            var channelId = (int)parsed[0];

            if (!_channelIdToHandler.ContainsKey(channelId))
            {
                return;
            }

            _channelIdToHandler[channelId](parsed, Configuration);
        }
    }
}
