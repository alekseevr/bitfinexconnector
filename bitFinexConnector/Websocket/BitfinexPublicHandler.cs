﻿using BitfinexConnector.Websocket.Messages;
using BitfinexConnector.Websocket.Responses;
using BitfinexConnector.Websocket.Responses.Candles;
using BitfinexConnector.Websocket.Responses.Configurations;
//using BitfinexConnector.Websocket.Responses.Trades;
using Newtonsoft.Json;

namespace BitfinexConnector.Websocket
{
    internal class BitfinexPublicHandler
    {
        private readonly BitfinexClientStreams _streams;
        private readonly BitfinexChannelList _channelIdToHandler;

        public BitfinexPublicHandler(BitfinexClientStreams streams, BitfinexChannelList channelIdToHandler)
        {
            _streams = streams;
            _channelIdToHandler = channelIdToHandler;
        }

        public void OnObjectMessage(string msg)
        {
            var parsed = JsonConvert.DeserializeObject<MessageBase>(msg);

            switch (parsed.Event)
            {
                case MessageType.Pong:
                    PongResponse.Handle(msg, _streams.PongSubject);
                    break;
                case MessageType.Error:
                    ErrorResponse.Handle(msg, _streams.ErrorSubject);
                    break;
                case MessageType.Info:
                    InfoResponse.Handle(msg, _streams.InfoSubject);
                    break;
                case MessageType.Subscribed:
                    OnSubscription(JsonConvert.DeserializeObject<SubscribedResponse>(msg));
                    break;
                case MessageType.Unsubscribed:
                    UnsubscribedResponse.Handle(msg, _streams.UnsubscriptionSubject);
                    break;
                //default:
                //    Log.Warning($"Missing handler for public stream, data: '{msg}'");
                //    break;
            }
        }

        private void OnSubscription(SubscribedResponse response)
        {

            var channelId = response.ChanId;

            // ********************
            // ADD HANDLERS BELOW
            // ********************

            switch (response.Channel)
            {
                case "trades":
                    //if pair is null means that is funding
                    if (response.Pair == null)
                    {
                        //_channelIdToHandler[channelId] = (data, config) =>
                        //    Funding.Handle(data, response, config, _streams.FundingsSubject);
                    }
                    else
                    {
                        _channelIdToHandler[channelId] = (data, config) =>
                            Trade.Handle(data, response, config, _streams.TradesSubject, _streams.TradesSnapshotSubject);
                    }
                    break;
                case "candles":
                    _channelIdToHandler[channelId] = (data, config) => 
                        Candles.Handle(data, response, _streams.CandlesSubject);

                    //response.Symbol = $"{response.Key.Split(':')[1]}:{response.Key.Split(':')[2]}"; - 
                    ///с таким Symbol можно будет подписываться 
                    //на несколько каналов candles с одинаковым pair и разным timeframe. 
                    //но и отписываться нужно будет также по ключу pair:timeframe
                    response.Symbol = response.Key.Split(':')[2];
                    response.Pair = response.Key.Split(':')[2];
                    break;
                //default:
                //    Log.Warning($"Missing subscription handler '{response.Channel}'");
                //    break;
            }

            _streams.SubscriptionSubject.OnNext(response);
        }

    }
}
