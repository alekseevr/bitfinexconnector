﻿namespace BitfinexConnector.Websocket.Requests.Subscriptions
{
    /// <summary>
    /// Subscribe to trades request
    /// </summary>
    public class TradesSubscribeRequest : SubscribeRequestBase
    {
        public TradesSubscribeRequest(string pair)
        {
            Symbol = pair;
        }

        public override string Channel => "trades";
        public string Symbol { get; }
    }
}
