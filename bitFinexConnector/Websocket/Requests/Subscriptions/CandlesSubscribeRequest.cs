﻿//using BitfinexConnector.Utils;

namespace BitfinexConnector.Websocket.Requests.Subscriptions
{
    public class CandlesSubscribeRequest : SubscribeRequestBase
    {
        public CandlesSubscribeRequest(string pair, string timeFrame)
        {

            Key = $"trade:{timeFrame}:{pair}";
        }

        public override string Channel => "candles";
        public string Key { get; set; }

    }
}
