﻿using System;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using BitfinexConnector.Websocket.Responses;
using BitfinexConnector.Websocket.Responses.Candles;
using BitfinexConnector.Websocket.Responses.Configurations;
//using BitfinexConnector.Websocket.Responses.Trades;

namespace BitfinexConnector.Websocket
{
    /// <summary>
    /// All provided streams from Bitfinex websocket API.
    /// You need to subscribe first, send subscription request (for example: `await client.Send(new TradesSubscribeRequest(pair))`)
    /// </summary>
    public class BitfinexClientStreams
    {
        internal readonly Subject<ErrorResponse> ErrorSubject = new Subject<ErrorResponse>();
        internal readonly Subject<InfoResponse> InfoSubject = new Subject<InfoResponse>();
        internal readonly Subject<PongResponse> PongSubject = new Subject<PongResponse>();
        internal readonly Subject<SubscribedResponse> SubscriptionSubject = new Subject<SubscribedResponse>();
        internal readonly Subject<UnsubscribedResponse> UnsubscriptionSubject = new Subject<UnsubscribedResponse>();

        internal readonly Subject<Trade> TradesSubject = new Subject<Trade>();
        internal readonly Subject<Trade[]> TradesSnapshotSubject = new Subject<Trade[]>();
        internal readonly Subject<Candles> CandlesSubject = new Subject<Candles>();


        /// <summary>
        /// Info about every occurred error
        /// </summary>
        public IObservable<ErrorResponse> ErrorStream => ErrorSubject.AsObservable();

        /// <summary>
        /// Initial info stream, publishes always on a new connection
        /// </summary>
        public IObservable<InfoResponse> InfoStream => InfoSubject.AsObservable();
        public IObservable<PongResponse> PongStream => PongSubject.AsObservable();

        /// <summary>
        /// Info about subscribed channel, you need to store channel id in order to future unsubscription
        /// </summary>
        public IObservable<SubscribedResponse> SubscriptionStream => SubscriptionSubject.AsObservable();

        /// <summary>
        /// Info about unsubscription
        /// </summary>
        public IObservable<UnsubscribedResponse> UnsubscriptionStream => UnsubscriptionSubject.AsObservable();


        public IObservable<Trade> TradesStream => TradesSubject.AsObservable();

        public IObservable<Trade[]> TradesSnapshotStream => TradesSnapshotSubject.AsObservable();


        /// <summary>
        /// Public candles stream for subscribed pair.
        /// Provides a way to access charting candle info
        /// </summary>
        public IObservable<Candles> CandlesStream => CandlesSubject.AsObservable();


        internal BitfinexClientStreams()
        {
        }
    }
}
