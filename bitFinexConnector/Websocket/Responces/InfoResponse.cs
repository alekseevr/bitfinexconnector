﻿using System.Reactive.Subjects;
using BitfinexConnector.Websocket.Messages;
using Newtonsoft.Json;

namespace BitfinexConnector.Websocket.Responses
{
    public class InfoResponse : MessageBase
    {
        public string Version { get; set; }
        public string Code { get; set; }
        public string Msg { get; set; }

        internal static void Handle(string msg, Subject<InfoResponse> subject)
        {
            var info = JsonConvert.DeserializeObject<InfoResponse>(msg);
            subject.OnNext(info);
        }
    }
}
