﻿using System.Reactive.Subjects;
using BitfinexConnector.Websocket.Messages;
using Newtonsoft.Json;

namespace BitfinexConnector.Websocket.Responses
{
    public class ErrorResponse : MessageBase
    {

        public string Code { get; set; }
        public string Msg { get; set; }

        internal static void Handle(string msg, Subject<ErrorResponse> subject)
        {
            var error = JsonConvert.DeserializeObject<ErrorResponse>(msg);
            subject.OnNext(error);
        }
    }
}
