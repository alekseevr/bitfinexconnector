﻿using System;
using System.Collections.Generic;
//using BitfinexConnector.Utils;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BitfinexConnector.Websocket.Responses.Candles
{
    class CandlesConverter : JsonConverter
    {
        public override bool CanWrite => false;

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            return JArrayToCandles(JArray.Load(reader));
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(Candle);
        }

        private Candles JArrayToCandles(JArray jArray)
        {
            var candles = new Candles();
            var candleList = new List<Candle>();

            if (jArray.Count==6)
            {
                candleList.Add(jArray.ToObject<Candle>());
            }
            else
            {
                foreach (var candle in jArray)
                {
                    candleList.Add(candle.ToObject<Candle>());
                }
            }

            candles.CandleList = candleList.ToArray();
            return candles;
        }
    }
}
