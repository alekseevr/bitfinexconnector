﻿using System.Reactive.Subjects;
//using BitfinexConnector.Utils;
using BitfinexConnector.Websocket.Responses;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BitfinexConnector.Websocket.Responses.Candles
{
    [JsonConverter(typeof(CandlesConverter))]
    public class Candles : ResponseBase
    {
        public Candle[] CandleList { get; set; }
        public string TimeFrame { get; set; }
        public string Pair { get; set; }

        internal static void Handle(JToken token, SubscribedResponse subscription, Subject<Candles> subject)
        {
            var data = token[1];

            if (data.Type != JTokenType.Array)
            {
                // probably heartbeat, ignore
                return;
            }

            //думаю, что параметры maxcount, from и to должны быть видны отсюда.
            //тогда мы сможем отфильтровать только нужные свечи и их количество в candles.candleList
            var candles = data.ToObject<Candles>();

            candles.TimeFrame = subscription.Key.Split(':')[1];
            //здесь можно не делать .Remove(0,1) и оставить тем самым в начале букву t.
            //точно такая же ситуация с t, как и в Trades (только там это выбор между Pair/Symbol).
            //candles.Pair = subscription.Key.Split(':')[2].Remove(0, 1);
            candles.Pair = subscription.Key.Split(':')[2];
            candles.ChanId = subscription.ChanId;
            subject.OnNext(candles);
        }
    }
}