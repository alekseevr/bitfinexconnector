﻿using System;
using System.Reactive.Subjects;
using BitfinexConnector.Json;
using BitfinexConnector.Websocket.Messages;
using Newtonsoft.Json;

namespace BitfinexConnector.Websocket.Responses
{
    public class PongResponse : MessageBase
    {
        public int Cid { get; set; }

        [JsonConverter(typeof(UnixDateTimeConverter))]
        public DateTimeOffset Ts { get; set; }


        internal static void Handle(string msg, Subject<PongResponse> subject)
        {
            var pong = JsonConvert.DeserializeObject<PongResponse>(msg);
            subject.OnNext(pong);
        }
    }
}
