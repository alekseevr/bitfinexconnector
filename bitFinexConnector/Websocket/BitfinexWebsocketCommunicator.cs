﻿using System;
using System.Net.WebSockets;
using Websocket.Client;

namespace BitfinexConnector.Websocket
{
    public class BitfinexWebsocketCommunicator : WebsocketClient, IWebsocketClient
    {
        public BitfinexWebsocketCommunicator(Uri url, Func<ClientWebSocket> clientFactory = null) 
            : base(url, clientFactory)
        {
        }
    }
}
