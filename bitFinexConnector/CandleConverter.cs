﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BitfinexConnector
{
    internal class CandleConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(Trade);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
            JsonSerializer serializer)
        {
            var array = JArray.Load(reader);
            return JArrayToTradingTicker(array);
        }

        public override bool CanWrite => false;

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        private Candle JArrayToTradingTicker(JArray array)
        {
            return new Candle
            {
                OpenTime = DateTimeOffset.FromUnixTimeMilliseconds((long)array[0]),
                OpenPrice = (decimal)array[1],
                ClosePrice = (decimal)array[2],
                HighPrice = (decimal)array[3],
                LowPrice = (decimal)array[4],
                TotalVolume = (decimal)array[5]
            };
        }
    }
}
