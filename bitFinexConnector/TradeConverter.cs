﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BitfinexConnector
{
    internal class TradeConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(Trade);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
            JsonSerializer serializer)
        {
            var array = JArray.Load(reader);
            return JArrayToTradingTicker(array);
        }

        public override bool CanWrite => false;

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        private Trade JArrayToTradingTicker(JArray array)
        {
            return new Trade
            {
                Id = array[0].ToString(),
                Time = DateTimeOffset.FromUnixTimeMilliseconds((long)array[1]),
                Amount = (decimal)array[2],
                Price = (decimal)array[3]
            };
        }
    }
}
