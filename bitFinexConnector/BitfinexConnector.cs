﻿using System;
using System.Collections.Generic;
using System.Collections;
using BitfinexConnector;
using System.Threading.Tasks;
using System.Net;
using System.Linq;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Threading;
using BitfinexConnector.Websocket;
using BitfinexConnector.Websocket.Requests.Subscriptions;
using BitfinexConnector.Websocket.Requests;

namespace BitFinexConnector
{
    public class BitfinexConnector : ITestConnector
    {
        public event Action<Trade> NewBuyTrade;
        public event Action<Candle> CandleSeriesProcessing;

        private BitfinexWebsocketCommunicator _communicator;
        private BitfinexWebsocketClient _client;

        //актуальный список каналов (подписок)
        private Hashtable _channelsDict = new Hashtable();
        public BitfinexConnector()
        {
            _communicator = new BitfinexWebsocketCommunicator(new Uri("wss://api.bitfinex.com/ws/2"));
            _communicator.Name = "Bitfinex-1";
            _communicator.ReconnectTimeoutMs = (int)TimeSpan.FromSeconds(300).TotalMilliseconds;
            _communicator.ReconnectionHappened.Subscribe(type =>
                Console.WriteLine($"Reconnection happened, type: {type}"));

            _client = new BitfinexWebsocketClient(_communicator);

            _client.Streams.InfoStream.Subscribe(info =>
            {
                Console.WriteLine($"Info received version: {info.Version}, reconnection happened, resubscribing to streams");
            });

            _client.Streams.SubscriptionStream.Subscribe(info =>
            {
                string key = $"{info.Channel}:{info.Symbol}";
                Console.WriteLine($"[{key}] --> {info.Event}, channelId - {info.ChanId}");

                _channelsDict.Add(key, info.ChanId);
            });

            _client.Streams.UnsubscriptionStream.Subscribe(x =>
            {
                Console.WriteLine($"[channelId {x.ChanId}] --> Unsubscribe status: {x.Status}");
            });

            _communicator.Start();
        }

        public async void SubscribeTrades(string pair, int maxCount = 100)
        {
            _client.Streams.TradesStream.Subscribe(trade => NewBuyTrade(trade));
            _client.Streams.TradesSnapshotStream.Subscribe(trades =>
            {
                foreach (var x in trades)
                {
                    NewBuyTrade(x);
                }
            });
            await _client.Send(new PingRequest() { Cid = 123456 });
            await _client.Send(new TradesSubscribeRequest(pair));
        }

        public async void SubscribeCandles(string pair, int periodInSec, DateTimeOffset? from = null, DateTimeOffset? to = null, long? count = 0)
        {
            _client.Streams.CandlesStream.Subscribe(candles =>
            {
                candles.CandleList.OrderBy(x => x.OpenTime).ToList().ForEach(x =>
                {
                    x.Pair = candles.Pair;
                    CandleSeriesProcessing(x);
                });
            });
            await _client.Send(new PingRequest() { Cid = 123456 });
            await _client.Send(new CandlesSubscribeRequest(pair, ConvertToTimeFrame(periodInSec)));
        }

        public async void UnsubscribeCandles(string pair)
        {
            string key = $"candles:{pair}";
            if (_channelsDict.ContainsKey(key))
            {
                //удаляем ключ из словаря сразу (подразумевается, что отписка пройдет успешно)
                _channelsDict.Remove(key);
                await _client.Send(new UnsubscribeRequest() { ChanId = (int)_channelsDict[key] });
            }
            else
                Console.WriteLine("Subscription data not found");
        }
        public void UnsubscribeAll()
        {
            foreach(DictionaryEntry channel in _channelsDict)
            {
                _client.Send(new UnsubscribeRequest() { ChanId = (int)channel.Value}).Wait();
            }
            //подразумевается, что все отписки пройдут успешно
            _channelsDict.Clear();
        }
        public async void UnsubscribeTrades(string pair)
        {
            string key = $"trades:{pair}";
            if (_channelsDict.ContainsKey(key))
            {
                //удаляем ключ из словаря сразу (подразумевается, что отписка пройдет успешно)
                _channelsDict.Remove(key);
                await _client.Send(new UnsubscribeRequest() { ChanId = (int)_channelsDict[key] });
            }
            else
                Console.WriteLine("Subscription data not found");
        }
        public Task<IEnumerable<Candle>> GetCandleSeriesAsync(string pair, int periodInSec, DateTimeOffset? from, DateTimeOffset? to = null, long? count = 0)
        {
            return Task.Run(async () =>
            {
                string strResponce = "";
                string req = $"https://api-pub.bitfinex.com/v2/candles/trade:{ConvertToTimeFrame(periodInSec)}:{pair}/hist?limit={count}&start={from}&end={to}";

                Task<string> responceGather = GetResponceString(req);
                strResponce = await responceGather;

                var candles = JsonConvert.DeserializeObject<JArray>(strResponce);
                List<Candle> listCandles = new List<Candle>();

                foreach (var c in candles)
                {
                    Candle candle = c.ToObject<Candle>();
                    //в сообщении отсутствует pair
                    //зато есть в параметрах функции (то же самое для trade)
                    candle.Pair = pair;
                    listCandles.Add(candle);
                }

                listCandles.Reverse();
                return listCandles.AsEnumerable();
            });
        }

        private Task<string> GetResponceString(string req)
        {
            return Task.Run(async () =>
            {
                HttpWebRequest request = WebRequest.Create(req) as HttpWebRequest;

                string strResponce = "";
                try
                {
                    HttpWebResponse response = (HttpWebResponse)await Task.Factory.FromAsync(request.BeginGetResponse,
                                                request.EndGetResponse,
                                                null);
                    StreamReader reader = new StreamReader(response.GetResponseStream());
                    strResponce = reader.ReadToEnd();
                    reader.Close();
                    return strResponce;
                }
                catch (WebException ex)
                {
                    StreamReader reader = new StreamReader(ex.Response.GetResponseStream());
                    strResponce = reader.ReadToEnd();
                    reader.Close();
                    throw new WebException(strResponce, ex);
                }
            });
        }

        public Task<IEnumerable<Trade>> GetNewTradesAsync(string pair, int maxCount)
        {
            return Task.Run(async () =>
            {
                string strResponce = "";

                Task<string> responceGather = GetResponceString($"https://api-pub.bitfinex.com/v2/trades/{pair}/hist?limit={maxCount}");
                strResponce = await responceGather;

                var trades = JsonConvert.DeserializeObject<JArray>(strResponce);
                List<Trade> listTrades = new List<Trade>();

                foreach (var t in trades)
                {
                    Trade trade = t.ToObject<Trade>();
                    //в сообщении отсутствует pair
                    //зато есть в параметрах функции (то же самое для candle)
                    trade.Pair = pair;
                    listTrades.Add(trade);
                }

                listTrades.Reverse();
                return listTrades.AsEnumerable();
            });
        }

        private string ConvertToTimeFrame(int periodInSec)
        {
            //если я правильно понял, мне требуется перевести получаемые через periodInSec числа 
            //в конкретные обозначения интервалов как в bitfinex api (я перевожу их с "округлением" вниз)
            
            //создал 2 списка, чтобы было наглядно
            var calc = new List<int>{ 2628000, 1209600, 604800, 86400, 43200, 21600, 10800, 3600, 1800, 900, 300, 60 };
            var table = new List<string> { "1M", "14D", "7D", "1D", "12h", "6h", "3h", "1h", "30m", "15m", "5m", "1m" };

            //создал dictionary из этих двух списков 
            var dictionary = calc.Zip(table, (k, v) => new { Key = k, Value = v })
                     .ToDictionary(x => x.Key, x => x.Value);

            //нашел первое значение "снизу" в упорядоченном списке значений и вернул его api обозначение
            foreach (var t in dictionary)
            {
                if (periodInSec >= t.Key)
                    return t.Value;
            }

            throw new Exception();
        }
    }
}
