﻿using System;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using BitfinexConnector.Websocket.Responses;
using BitfinexConnector.Websocket.Responses.Configurations;
using System.Reactive.Subjects;
using BitfinexConnector.Websocket.Responses.Trades;

namespace BitfinexConnector
{
    [JsonConverter(typeof(TradeConverter))]
    public class Trade
    {
        /// <summary>
        /// Валютная пара
        /// </summary>
        public string Pair { get; set; }

        /// <summary>
        /// Цена трейда
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Объем трейда
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Направление (buy/sell)
        /// </summary>
        /// Side нигде не используется - удалил
        //public string Side { get; set; }

        /// <summary>
        /// Время трейда
        /// </summary>
        public DateTimeOffset Time { get; set; }

        //добавил Type, т.к. его он есть в сообщении
        public TradeType Type { get; set; }
        /// <summary>
        /// Id трейда
        /// </summary>
        public string Id { get; set; }

        //расширил класс Static методом Handle (для Websocket клиента, паттерн "наблюдатель")
        internal static void Handle(JToken token, SubscribedResponse subscription, ConfigurationState config,
    Subject<Trade> subject, Subject<Trade[]> subjectSnapshot)
        {
            var firstPosition = token[1];
            if (firstPosition.Type == JTokenType.Array)
            {
                // initial snapshot
                Handle(token, firstPosition.ToObject<Trade[]>(), subscription, config, subjectSnapshot);
                return;
            }

            var tradeType = TradeType.Executed;
            if (firstPosition.Type == JTokenType.String)
            {
                if ((string)firstPosition == "tu")
                    tradeType = TradeType.UpdateExecution;
                else if ((string)firstPosition == "hb")
                    return; // heartbeat, ignore
            }

            var data = token[2];
            if (data.Type != JTokenType.Array)
            {
                // bad format, ignore
                return;
            }

            var trade = data.ToObject<Trade>();
            trade.Type = tradeType;
            //здесь нужно выбрать между subscription.Pair и Symbol (в Symbol есть префикс t).
            //я выбрал Symbol.
            trade.Pair = subscription.Symbol;
            subject.OnNext(trade);
        }

        internal static void Handle(JToken token, Trade[] trades, SubscribedResponse subscription, ConfigurationState config, Subject<Trade[]> subject)
        {
            var reversed = trades.Reverse().ToArray(); // newest last

            //maxcount должен быть виден отсюда
            foreach (var trade in reversed)
            {
                trade.Type = TradeType.Executed;
                //здесь нужно выбрать между subscription.Pair и Symbol (в Symbol есть префикс t).
                //я выбрал Symbol.
                trade.Pair = subscription.Symbol;
            }
            subject.OnNext(reversed);
        }
    }
}
