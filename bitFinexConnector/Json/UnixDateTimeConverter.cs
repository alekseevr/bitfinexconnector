﻿using System;
using System.Globalization;
//using BitfinexConnector.Utils;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace BitfinexConnector.Json
{
    public class UnixDateTimeConverter : DateTimeConverterBase
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var substracted = ((DateTimeOffset)value).ToUnixTimeMilliseconds();
            writer.WriteRawValue(substracted.ToString(CultureInfo.InvariantCulture));
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.Value == null) { return null; }
            return DateTimeOffset.FromUnixTimeMilliseconds((long)reader.Value);
        }
    }
}
